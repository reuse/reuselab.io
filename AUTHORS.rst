..
  Copyright (C) 2017  Free Software Foundation Europe e.V.
  Copyright (C) 2017  Sebastian Schuberth <schuberth@fsfe.org>
  Copyright (C) 2018  Carmen Bianca Bakker <carmenbianca@fsfe.org>

  This file is part of reuse, available from its original location:
  <https://gitlab.com/reuse/reuse/>.

  reuse is free software: you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  reuse is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  reuse.  If not, see <http://www.gnu.org/licenses/>.

  SPDX-License-Identifier: CC-BY-SA-4.0

=======
Credits
=======

Development Lead
----------------

- Carmen Bianca Bakker <carmenbianca@fsfe.org>

Contributors
------------

- Sebastian Schuberth <schuberth@fsfe.org>

Translators
-----------

- Dutch:

  + André Ockers <ao@fsfe.org>

  + Carmen Bianca Bakker <carmenbianca@fsfe.org>

- Esperanto:

  + Carmen Bianca Bakker <carmenbianca@fsfe.org>

  + Tirifto <tirifto@posteo.cz>

- Spanish:

  + flow <adolflow@sindominio.net>

  + pd <euklade@gmail.com>
