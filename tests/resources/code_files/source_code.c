/* Copyright (C) 2017  Free Software Foundation Europe e.V. */

/* SPDX-License-Identifier: {{ license }} */

#include <stdio.h>

main()
{
    printf("Hello, world!\n");
}
